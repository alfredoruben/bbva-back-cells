package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class DemoApplication /*extends SpringBootServletInitializer*/ {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Bean
	public WebMvcConfigurer corsConfigurer() {
	   return new WebMvcConfigurer() {
		@Override
		public void addCorsMappings(CorsRegistry registry) {
			registry.addMapping("/**").allowedOrigins("*")
//			.allowedMethods("PUT", "DELETE")
//			.allowedHeaders("header1", "header2", "header3")
//			.exposedHeaders("header1", "header2")
//			.allowCredentials(false)
			;
			
			WebMvcConfigurer.super.addCorsMappings(registry);
		}
	};
	}
}

/*
 * http://www.technicalkeeda.com/spring-tutorials/spring-4-mongodb-repository-example
 * https://www.mongodb.org/
 * https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/
 * https://www.baeldung.com/spring-data-mongodb-tutorial
 * https://docs.mongodb.com/manual/mongo/
 */