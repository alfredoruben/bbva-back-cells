package com.example.demo.service;

import com.example.demo.model.UsuarioSesion;

public interface UsuarioService {
	public UsuarioSesion validaLogin(String dni,String clave);
}
