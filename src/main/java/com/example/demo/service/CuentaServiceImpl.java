package com.example.demo.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Cuenta;
import com.example.demo.model.Movimiento;
import com.example.demo.repository.CuentaRepository;
import com.example.demo.repository.MovimientoRepository;

@Repository
public class CuentaServiceImpl implements CuentaService {

	@Autowired
	private CuentaRepository cuentaRepository;
	@Autowired
	private MovimientoRepository movimientoRepository;

	@Override
	public List<Cuenta> listaCuentaByCliente(String idCliente) {
		List<Cuenta> lista = cuentaRepository.findByIdCliente(idCliente);
		for (Cuenta cuenta : lista) {
			List<Movimiento> listaMovi = movimientoRepository.findByIdCuenta(cuenta.getId());
			BigDecimal monto = new BigDecimal(0);
			for (Movimiento movimiento : listaMovi) {
				monto=monto.add(movimiento.getValor());
			}
			cuenta.setSaldo(monto);
		}
		return lista;
	}

}
