package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Cuenta;

public interface CuentaService {
	public List<Cuenta> listaCuentaByCliente(String idCliente);
}
