package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Cliente;
import com.example.demo.model.Cuenta;
import com.example.demo.model.UsuarioSesion;
import com.example.demo.repository.ClienteRepository;
@Repository
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private ClienteRepository clienteRepository;
	@Override
	public UsuarioSesion validaLogin(String dni, String clave) {
		List<Cliente> cliente=clienteRepository.findByDniAndClave(dni, clave);
		UsuarioSesion usuarioSesion=null;
		if(cliente!=null && cliente.size()>0){
			//cargar cuentas
			List<Cuenta> listaCuenta=cuentaService.listaCuentaByCliente(cliente.get(0).getId());
			usuarioSesion=new UsuarioSesion();
			usuarioSesion.setId(cliente.get(0).getId());
			usuarioSesion.setFirstName(cliente.get(0).getFirstName());
			usuarioSesion.setLastName(cliente.get(0).getLastName());
			usuarioSesion.setListaCuenta(listaCuenta);
		}		
		return usuarioSesion;
	}

}
