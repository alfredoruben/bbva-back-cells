package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Cliente;
import com.example.demo.model.UsuarioSesion;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.service.UsuarioService;

@RestController
public class ClienteController
{
	@Autowired
	ClienteRepository employeeRepository;
	@Autowired
	UsuarioService usuarioService;

	@GetMapping(value = "/", produces = "application/json; charset=utf-8")
	public String getHealthCheck()
	{
		return "{ \"isWorking\" : true }";
	}

	@GetMapping("/clientes")
	public List<Cliente> getEmployees()
	{
		List<Cliente> employeesList = employeeRepository.findAll();
		return employeesList;
	}

	@GetMapping("/cliente/{id}")
	public Optional<Cliente> getEmployee(@PathVariable String id)
	{
		Optional<Cliente> emp = employeeRepository.findById(id);
		return emp;
	}

	@PutMapping("/cliente/{id}")
	public Optional<Cliente> updateEmployee(@RequestBody Cliente newEmployee, @PathVariable String id)
	{
		Optional<Cliente> optionalEmp = employeeRepository.findById(id);
		if (optionalEmp.isPresent()) {
			Cliente emp = optionalEmp.get();
			emp.setFirstName(newEmployee.getFirstName());
			emp.setLastName(newEmployee.getLastName());
			if(newEmployee.getClave()!=null){
				emp.setClave(newEmployee.getClave());
			}
			if(newEmployee.getDni()!=null){
				emp.setDni(newEmployee.getDni());
			}
			employeeRepository.save(emp);
		}
		return optionalEmp;
	}

	@DeleteMapping(value = "/cliente/{id}", produces = "application/json; charset=utf-8")
	public String deleteCliente(@PathVariable String id) {
		Boolean result = employeeRepository.existsById(id);
		employeeRepository.deleteById(id);
		return "{ \"success\" : "+ (result ? "true" : "false") +" }";
	}

	@PostMapping("/cliente")
	public Cliente addEmployee(@RequestBody Cliente newEmployee)
	{
		String id = String.valueOf(new Random().nextInt());
		newEmployee.setId(id);
		employeeRepository.insert(newEmployee);
		return newEmployee;
	}
	@PostMapping("/cliente/login")
	public UsuarioSesion login(@RequestBody Cliente employee)
	{
		UsuarioSesion usuarioSesion=usuarioService.validaLogin(employee.getDni(), employee.getClave());
		return usuarioSesion;
	}
}
