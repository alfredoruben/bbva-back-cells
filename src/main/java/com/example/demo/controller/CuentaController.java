package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Cuenta;
import com.example.demo.repository.CuentaRepository;
import com.example.demo.service.CuentaService;

@RestController
public class CuentaController
{
	@Autowired
	CuentaRepository cuentaRepository;
	@Autowired
	CuentaService cuentaService;

	@GetMapping("/cuentas")
	public List<Cuenta> getCuentas()
	{
		List<Cuenta> cuentasList = cuentaRepository.findAll();
		return cuentasList;
	}

	@GetMapping("/cuenta/{id}")
	public Optional<Cuenta> getCuenta(@PathVariable String id)
	{
		Optional<Cuenta> emp = cuentaRepository.findById(id);
		return emp;
	}

	@PutMapping("/cuenta/{id}")
	public Optional<Cuenta> updateEmployee(@RequestBody Cuenta newCuenta, @PathVariable String id)
	{
		Optional<Cuenta> optionalEmp = cuentaRepository.findById(id);
		if (optionalEmp.isPresent()) {
			Cuenta emp = optionalEmp.get();
			emp.setDenominacion(newCuenta.getDenominacion());
			emp.setNumero(newCuenta.getNumero());
			emp.setNombre(newCuenta.getNombre());
			emp.setIdCliente(newCuenta.getIdCliente());
			cuentaRepository.save(emp);
		}
		return optionalEmp;
	}

	@DeleteMapping(value = "/cuenta/{id}", produces = "application/json; charset=utf-8")
	public String deleteCuenta(@PathVariable String id) {
		Boolean result = cuentaRepository.existsById(id);
		cuentaRepository.deleteById(id);
		return "{ \"success\" : "+ (result ? "true" : "false") +" }";
	}

	@PostMapping("/cuenta")
	public Cuenta addCuenta(@RequestBody Cuenta newCuenta)
	{
		String id = String.valueOf(new Random().nextInt());
		newCuenta.setId(id);
		cuentaRepository.insert(newCuenta);
		return newCuenta;
	}
	@GetMapping("/cuenta/cliente/{idCliente}")
	public List<Cuenta> getCuentasByCliente(@PathVariable String idCliente)
	{
		List<Cuenta> cuentasList = cuentaService.listaCuentaByCliente(idCliente);
		return cuentasList;
	}
}
