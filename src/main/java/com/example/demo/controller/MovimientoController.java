package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Movimiento;
import com.example.demo.repository.MovimientoRepository;

@RestController
public class MovimientoController
{
	@Autowired
	MovimientoRepository movimientoRepository;



	@GetMapping("/movimientos")
	public List<Movimiento> getMovimientos()
	{
		List<Movimiento> movimientosList = movimientoRepository.findAll();
		return movimientosList;
	}

	@GetMapping("/movimiento/{id}")
	public Optional<Movimiento> getMovimiento(@PathVariable String id)
	{
		Optional<Movimiento> emp = movimientoRepository.findById(id);
		return emp;
	}

	@PutMapping("/movimiento/{id}")
	public Optional<Movimiento> updateEmployee(@RequestBody Movimiento newMovimiento, @PathVariable String id)
	{
		Optional<Movimiento> optionalEmp = movimientoRepository.findById(id);
		if (optionalEmp.isPresent()) {
			Movimiento emp = optionalEmp.get();
			emp.setDescripcion(newMovimiento.getDescripcion());
			emp.setValor(newMovimiento.getValor());
			movimientoRepository.save(emp);
		}
		return optionalEmp;
	}

	@DeleteMapping(value = "/movimiento/{id}", produces = "application/json; charset=utf-8")
	public String deleteEmployee(@PathVariable String id) {
		Boolean result = movimientoRepository.existsById(id);
		movimientoRepository.deleteById(id);
		return "{ \"success\" : "+ (result ? "true" : "false") +" }";
	}

	@PostMapping("/movimiento")
	public Movimiento addMovimiento(@RequestBody Movimiento newMovimiento)
	{
		String id = String.valueOf(new Random().nextInt());
		newMovimiento.setId(id);
		newMovimiento.setFecha(new Date());
		movimientoRepository.insert(newMovimiento);
		return newMovimiento;
	}
	@GetMapping("/movimientos/{idCuenta}")
	public List<Movimiento> getMovimientosByCuenta(@PathVariable String idCuenta)
	{
		List<Movimiento> movimientosList = movimientoRepository.findByIdCuenta(idCuenta);
		return movimientosList;
	}
}
