package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Cliente;

public interface ClienteRepository extends MongoRepository<Cliente, String> {
	public List<Cliente> findByDniAndClave(String dni,String clave);
	
}
