package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Cuenta;

public interface CuentaRepository extends MongoRepository<Cuenta, String> {
	public List<Cuenta> findByIdCliente(String idCliente);
}
