package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Movimiento;

public interface MovimientoRepository extends MongoRepository<Movimiento, String> {
	public List<Movimiento> findByIdCuenta(String idCuenta);
}
