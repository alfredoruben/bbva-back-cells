package com.example.demo.model;

import java.util.List;

public class UsuarioSesion {
	private String id;
	private String firstName;
	private String lastName;
	private List<Cuenta> listaCuenta;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<Cuenta> getListaCuenta() {
		return listaCuenta;
	}
	public void setListaCuenta(List<Cuenta> listaCuenta) {
		this.listaCuenta = listaCuenta;
	}
	
}
